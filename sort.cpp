#include <iostream>
#include <vector>

#include "util.h"
#include "generate_file.h"

using namespace std;

// calculate the number of strings that can fit in memory:
// block - set of strings that we will load from file to memory
// (max number of strings that can fit in memory)
// strs_in_block - number of strings that can fit in memory
// strs_in_block = memory_size / max_string_len
// num_blocks - number of blocks in the file
uint64_t MaxStringLen(const string& filename, uint64_t& num_blocks, uint64_t& strs_in_block,
                      uint64_t memory_size) {
    fstream file(filename, ios::in);

    size_t max_len = 0;
    uint64_t count = 0;

    // calculate max string length
    string line;
    while (getline(file, line)) {
        max_len = max(max_len, line.size());
        ++count;
    }

    strs_in_block = memory_size / max_len;
    if (strs_in_block % 2) {
        --strs_in_block;
    }

    // add empty lines to file for completeness of the last block
    // and remember this number
    int num_add_spaces = 0;
    if (count % strs_in_block != 0) {
        fstream file(filename, ios::out | ios::app);

        num_add_spaces = strs_in_block - (count % strs_in_block);
        file.write(RepeatSymbol('\n', num_add_spaces - 1).c_str(), num_add_spaces);

        count += num_add_spaces;
    }

    num_blocks = count / strs_in_block;

    return num_add_spaces;
}

// sort file blocks
// load the blocks into memory one by one and sort it in-place (merge sort)
// and return block to the file
void FirstSortByMaxBlocks(fstream& file, fstream& merge_file, uint64_t num_blocks,
                          uint64_t strs_in_block) {
    uint64_t offset = 0;
    string line;
    vector<string> store;

    file.seekg(0);

    for (uint64_t j = 0; j < num_blocks; ++j) {
        // load
        for (uint64_t i = 0; i < strs_in_block; ++i) {
            getline(file, line);
            store.push_back(line);
        }
        // sort
        MergeSortInplace(store.begin(), store.end());

        string join = JoinStrings(store.begin(), store.end(), '\n');

        // return to the file
        merge_file.seekp(offset);
        merge_file.write(join.c_str(), join.size());
        offset += join.size();

        store.clear();
    }
}

// divide blocks in half
// the first half of the first block load to the memory (half of the memory - empty)
// one by one load to the memory others halves of blocks and do merge with the first half of memory
// (vector) at the end the first half of vector contains the current minimum (set of strings) and
// this set we save to file - this is  part of the result
void SortByHalfBlocks(fstream& file, fstream& merge_file, uint64_t num_half_blocks,
                      uint64_t strs_in_block, uint64_t& ready_offset) {

    uint64_t offset_write = 0;
    uint64_t strs_in_half = strs_in_block / 2;
    string line;
    vector<string> store;

    merge_file.seekg(0);

    uint64_t offset_read = 0;

    // load first half of the first block
    for (uint64_t j = 0; j < strs_in_half; ++j) {
        getline(merge_file, line);
        store.push_back(line);
        offset_read += line.size() + 1;
    }

    // merge with others halves
    for (uint64_t i = 0; i < num_half_blocks - 1; ++i) {
        merge_file.seekg(offset_read);
        for (uint64_t j = 0; j < strs_in_half; ++j) {
            getline(merge_file, line);
            store.push_back(line);
            offset_read += line.size() + 1;
        }
        MergeHalves(store);

        // join the second half of strings with move from the vector
        string join = JoinStrings(store.begin() + strs_in_half, store.end(), '\n');

        merge_file.seekp(offset_write);
        merge_file.write(join.c_str(), join.size());
        offset_write += join.size();

        store.erase(store.begin() + strs_in_half, store.end());
    }

    // save to file part of the result
    string join = JoinStrings(store.begin(), store.begin() + strs_in_half, '\n');
    file.seekp(ready_offset);
    file.write(join.c_str(), join.size());
    ready_offset += join.size();
}

void ExternalSort(const string& filename, uint64_t memory_size) {
    fstream file(filename, ios::in | ios::out);
    fstream merge_file("merge_file", ios::in | ios::out | ios::trunc);

    // define number of blocks and number of strings in the block
    uint64_t num_blocks, strs_in_block;
    uint64_t num_add_spaces = MaxStringLen(filename, num_blocks, strs_in_block, memory_size);

    string line;
    vector<string> vec;

    uint64_t num_half_blocks = num_blocks * 2;
    uint64_t ready_offset = 0;

    FirstSortByMaxBlocks(file, merge_file, num_blocks, strs_in_block);
    while (num_half_blocks > 0) {
        SortByHalfBlocks(file, merge_file, num_half_blocks, strs_in_block, ready_offset);
        --num_half_blocks;
    }

    // delete empty lines from file that we added
    if (num_add_spaces) {
        file.seekp(0);
        file.write(RepeatSymbol(' ', num_add_spaces - 1).c_str(), num_add_spaces);
    }

    file.close();
    merge_file.close();

    remove("merge_file");
}

int main() {
    string file_name = GenerateFileOnlyLetters(20050, 25);
    ExternalSort(file_name, 1024 * 10);
    return 0;
}
