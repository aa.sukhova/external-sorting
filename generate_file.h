#pragma once

#include <random>
#include <string>
#include <fstream>

using namespace std;

string GenerateFileOnlyLetters(uint64_t num_str, int max_len) {
    string name = "random_letters_file";
    fstream file(name, ios::out | ios::trunc);

    uniform_int_distribution<int> dist_char(96, 122);
    uniform_int_distribution<int> dist_len(1, max_len);

    mt19937 generator(1234);

    for (uint64_t i = 0; i < num_str; ++i) {
        string line;
        int len = dist_len(generator);

        for (int i = 0; i < len; ++i) {
            char ch = dist_char(generator);
            if (ch == 96) {
                ch = ' ';
            }
            line += ch;
        }
        line += '\n';

        file.write(line.c_str(), line.size());
    }

    return name;
}

string GenerateRandomFile(uint64_t num_str, int max_len) {
    string name = "random_file";
    fstream file(name, ios::out | ios::trunc);

    uniform_int_distribution<int> dist_char(32, 122);
    uniform_int_distribution<int> dist_len(0, max_len);

    mt19937 generator(1234);

    for (uint64_t i = 0; i < num_str; ++i) {
        string line;
        int len = dist_len(generator);

        for (int i = 0; i < len; ++i) {
            char ch = dist_char(generator);
            if (ch == 47) {
                ch = ' ';
            }
            line += ch;
        }
        line += '\n';

        file.write(line.c_str(), line.size());
    }

    return name;
}
