#pragma once

#include <string>
#include <algorithm>
#include <vector>

using namespace std;

string RepeatSymbol(char symbol, int count) {
    string res;
    res.insert(0, count, symbol);
    return res + '\n';
}

void MergeHalves(vector<string>& vec) {
    uint64_t half = vec.size() / 2;
    inplace_merge(vec.begin(), vec.begin() + half, vec.end());
}

string JoinStrings(vector<string>::iterator begin, vector<string>::iterator end, char delimiter) {
    string res;
    for (auto it = begin; it != end; ++it) {
        res += std::move(*it) + delimiter;
    }
    // for_each(begin, end, [&res, delimiter](string& s) { res += s + delimiter; });
    return res;
}

void MergeSortInplace(vector<string>::iterator begin, vector<string>::iterator end) {
    if (end - begin > 1) {
        auto middle = begin + (end - begin) / 2;
        MergeSortInplace(begin, middle);
        MergeSortInplace(middle, end);
        inplace_merge(begin, middle, end);
    }
}
